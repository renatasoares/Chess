package control;

import model.Square;
import model.Piece;
import java.awt.Color;
import java.util.ArrayList;
import model.Square.SquareEventListener;
import java.awt.Point;

public class SquareControl implements SquareEventListener {

	public static final int ROW_NUMBER = 8;
	public static final int COL_NUMBER = 8;

	public static final Color DEFAULT_COLOR_ONE = Color.WHITE;
	public static final Color DEFAULT_COLOR_TWO = Color.GRAY;
	public static final Color DEFAULT_COLOR_HOVER = Color.BLUE;
	public static final Color DEFAULT_COLOR_SELECTED = Color.GREEN;
	public static final Color DEFAULT_COLOR_MOVED = Color.GREEN;

	public static final Square EMPTY_SQUARE = null;

	private Color colorOne;
	private Color colorTwo;
	private Color colorHover;
	private Color colorSelected;
	private Color colorMoves;

	private Square selectedSquare;
	private ArrayList<Square> squareList;
	private ArrayList<Square> possibleSquares;

	public SquareControl() {
		this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,
				DEFAULT_COLOR_SELECTED,DEFAULT_COLOR_MOVED);
	}

	public SquareControl(Color colorOne, Color colorTwo, Color colorHover,
			Color colorSelected, Color colorMoves) {
		this.colorOne = colorOne;
		this.colorTwo = colorTwo;
		this.colorHover = colorHover;
		this.colorSelected = colorSelected;
		this.colorMoves = colorMoves;

		this.squareList = new ArrayList<>();
		this.possibleSquares = new ArrayList<>();
		
		createSquares();
	}

	public void resetColor(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.setColor(getGridColor(row, col));
	}

	@Override
	public void onHoverEvent(Square square) {
		square.setColor(this.colorHover);
	}


	@Override
	public void onSelectEvent(Square square) {
		if (haveSelectedCellPanel()) {
			if(!possibleSquares.contains(square) && !this.selectedSquare.equals(square)){
				System.out.println ("ERRO");
			}
			if (!this.selectedSquare.equals(square) && possibleSquares.contains(square)) { 
				moveContentOfSelectedSquare(square);
				possibleSquares.clear();
			} 
			else {
				unselectSquare(square);
			}
		} 
		else {
			selectSquare(square);
		}
}

	public void onOutEvent(Square square) {
		if (this.selectedSquare != square && !possibleSquares.contains(square)) {
			resetColor(square);
		} else {
			if(this.selectedSquare == square)
				square.setColor(this.colorSelected);
			else
				square.setColor(Color.GREEN);
		}
	}

	public Square getSquare(int row, int col) {
		return this.squareList.get((row * COL_NUMBER) + col);
	}

	public ArrayList<Square> getSquareList() {
		return this.squareList;
	}

	public Color getGridColor(int row, int col) {
		if ((row + col) % 2 == 0) {
			return this.colorOne;
		} else {
			return this.colorTwo;
		}
	}

	private void addSquare() {
		Square square = new Square();
		this.squareList.add(square);
		resetColor(square);
		resetPosition(square);
		square.setSquareEventListener(this);
	}

	private void resetPosition(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.getPosition().setLocation(row, col);
	}

	private boolean haveSelectedCellPanel() {
		return this.selectedSquare != EMPTY_SQUARE;
	}

	private void moveContentOfSelectedSquare(Square square) {
		square.setPiece(this.selectedSquare.getPiece());
		this.selectedSquare.removePiece();
		unselectSquare(square);
	}

	private void selectSquare(Square square) {
		if (square.havePiece()) {
			this.selectedSquare = square;
			this.selectedSquare.setColor(this.colorSelected);
			this.showMoves(this.selectedSquare);
		}
	}

	private void unselectSquare(Square square) {
		resetColor(this.selectedSquare);
		
		for(int i = 0; i < possibleSquares.size(); i++){
				resetColor(possibleSquares.get(i));
		}
		
		possibleSquares.clear();
		this.selectedSquare = EMPTY_SQUARE;			
				
	}

	private void createSquares() {
		for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
			addSquare();
		}
	}
	
	private void BishopMoves(Square square){
		
		ArrayList<Point> bishopMoves = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);

			for(int i = 0; i < bishopMoves.size(); i++){
			
				Square showSquares = getSquare(bishopMoves.get(i).x, bishopMoves.get(i).y);
				
					showSquares.setColor(this.colorMoves);
					possibleSquares.add(showSquares);
			
		}		
	}
	private void KingMoves(Square square){
		
		ArrayList<Point> KingMoves = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		
		if (this.selectedSquare.getPiece().getPieceColor() == "BROWN"){
			for(int i = 0; i < KingMoves.size(); i++){
			
				Square showSquares = getSquare(KingMoves.get(i).x, KingMoves.get(i).y);
				if (showSquares.havePiece()){
					if (showSquares.getPiece().getPieceColor() == ("WHITE")){
						showSquares.setColor(this.colorMoves);
						possibleSquares.add(showSquares);
						
					}
				}
				else{
					showSquares.setColor(this.colorMoves);
					possibleSquares.add(showSquares);
				}
			}	
		}	
		if (this.selectedSquare.getPiece().getPieceColor() == "WHITE"){
			for(int i = 0; i < KingMoves.size(); i++){
			
				Square showSquares = getSquare(KingMoves.get(i).x, KingMoves.get(i).y);
				if (showSquares.havePiece()){
					if (showSquares.getPiece().getPieceColor() == ("BROWN")){
						showSquares.setColor(this.colorMoves);
						possibleSquares.add(showSquares);
						
					}
				}
				else{
					showSquares.setColor(this.colorMoves);
					possibleSquares.add(showSquares);
				}
			}	
		}		
	}
	private void RookMoves(Square square){
		ArrayList<Point> RookMoves = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		for(int i = 0; i < RookMoves.size(); i++){
		
			Square showSquares = getSquare(RookMoves.get(i).x, RookMoves.get(i).y);
			
				showSquares.setColor(this.colorMoves);
				possibleSquares.add(showSquares);
		
	}	

	}
	private void QueenMoves(Square square){
		ArrayList<Point> QueenMoves = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);

		for(int i = 0; i < QueenMoves.size(); i++){
		
			Square showSquares = getSquare(QueenMoves.get(i).x, QueenMoves.get(i).y);
			
				showSquares.setColor(this.colorMoves);
				possibleSquares.add(showSquares);
		
		}	
	}	
	private void PawnWMoves(Square square){
		
		ArrayList<Point> PawnWMoves = square.getPiece().getMovesW(square.getPosition().x, square.getPosition().y);
		
		if (this.selectedSquare.getPiece().getPieceColor() == "WHITE"){
			for(int i = 0; i < PawnWMoves.size(); i++){
			
				Square showSquares = getSquare(PawnWMoves.get(i).x, PawnWMoves.get(i).y);
				if (showSquares.havePiece()){
					if (showSquares.getPiece().getPieceColor() == ("BROWN")){
						showSquares.setColor(this.colorMoves);
						possibleSquares.add(showSquares);
						
					}
				}
				else{
					showSquares.setColor(this.colorMoves);
					possibleSquares.add(showSquares);
				}
			}	
		}
	}
	private void PawnBMoves(Square square){
		
		ArrayList<Point> PawnBMoves = square.getPiece().getMovesB(square.getPosition().x, square.getPosition().y);
		if (this.selectedSquare.getPiece().getPieceColor() == "BROWN"){
			for(int i = 0; i < PawnBMoves.size(); i++){
			
				Square showSquares = getSquare(PawnBMoves.get(i).x, PawnBMoves.get(i).y);
				if (showSquares.havePiece()){
					if (showSquares.getPiece().getPieceColor() == ("WHITE")){
						showSquares.setColor(this.colorMoves);
						possibleSquares.add(showSquares);
						
					}
				}
				else{
					showSquares.setColor(this.colorMoves);
					possibleSquares.add(showSquares);
				}
			}	
		}
	}
	private void KnightMoves(Square square){
		
		ArrayList<Point> KnightMoves = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		if (this.selectedSquare.getPiece().getPieceColor() == "BROWN"){
			for(int i = 0; i < KnightMoves.size(); i++){
			
				Square showSquares = getSquare(KnightMoves.get(i).x, KnightMoves.get(i).y);
				if (showSquares.havePiece()){
					if (showSquares.getPiece().getPieceColor() == ("WHITE")){
						showSquares.setColor(this.colorMoves);
						possibleSquares.add(showSquares);
						
					}
				}
				else{
					showSquares.setColor(this.colorMoves);
					possibleSquares.add(showSquares);
				}
			}	
		}	
		if (this.selectedSquare.getPiece().getPieceColor() == "WHITE"){
			for(int i = 0; i < KnightMoves.size(); i++){
			
				Square showSquares = getSquare(KnightMoves.get(i).x, KnightMoves.get(i).y);
				if (showSquares.havePiece()){
					if (showSquares.getPiece().getPieceColor() == ("BROWN")){
						showSquares.setColor(this.colorMoves);
						possibleSquares.add(showSquares);
						
					}
				}
				else{
					showSquares.setColor(this.colorMoves);
					possibleSquares.add(showSquares);
				}
			}	
		}
	}


	private void showMoves(Square square){
		
		Piece piece = square.getPiece();
		
		if(piece.getPieceName().equalsIgnoreCase("BishopB") || piece.getPieceName().equalsIgnoreCase("BishopW")) {
			BishopMoves(square);
			System.out.println ("Movimento da peca:");
			System.out.println (piece.getPieceName());
		}		
		if(piece.getPieceName().equalsIgnoreCase("KingB") || piece.getPieceName().equalsIgnoreCase("KingW")){
			KingMoves(square);
			System.out.println ("Movimento da peca:");
			System.out.println (piece.getPieceName());
		}
		if(piece.getPieceName().equalsIgnoreCase("RookB") || piece.getPieceName().equalsIgnoreCase("RookW")){
			RookMoves(square);
			System.out.println ("Movimento da peca:");
			System.out.println (piece.getPieceName());
		}
		if(piece.getPieceName().equalsIgnoreCase("QueenB") || piece.getPieceName().equalsIgnoreCase("QueenW")){
			QueenMoves(square);
			System.out.println ("Movimento da peca:");
			System.out.println (piece.getPieceName());
		}
		if(piece.getPieceName().equalsIgnoreCase("PawnW")){
			PawnWMoves(square);
			System.out.println ("Movimento da peca:");
			System.out.println (piece.getPieceName());
		}
		if(piece.getPieceName().equalsIgnoreCase("PawnB")){
			PawnBMoves(square);
			System.out.println ("Movimento da peca:");
			System.out.println (piece.getPieceName());
		}
		if(piece.getPieceName().equalsIgnoreCase("KnightW")||piece.getPieceName().equalsIgnoreCase("KnightB")){
			KnightMoves(square);
			System.out.println ("Movimento da peca:");
			System.out.println (piece.getPieceName());
		}
	}
}