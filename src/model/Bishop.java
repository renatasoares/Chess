package model;

import java.util.ArrayList;
import java.awt.Point;

public class Bishop extends Piece {

	public Bishop(String name, String piecePath) {
			super(name, piecePath);
	}
	public ArrayList<Point> getMoves(int x,int y){
		ArrayList<Point> bishopMoves = new ArrayList<>();
		
		for(int x2=x,y2=y; x2<7 || y2>0; x2++,y2--) {
			if(x2==7 || y2==0)
				break;
			
			Point bishopPoint = new Point(x2+1, y2-1);
			bishopMoves.add(bishopPoint);			
		}
		
		for(int x2=x,y2=y; x2<7 || y2<7 ; x2++,y2++) {
			if(x2==7 || y2==7)
				break;		
						
			Point bishopPoint = new Point(x2+1, y2+1);
			bishopMoves.add(bishopPoint);			
		}
		for(int x2 = x, y2 = y; x2>0 || y2>0; x2--, y2--) {
			if(x2==0 || y2==0)
				break;

			Point bishopPoint = new Point(x2-1, y2-1);
			bishopMoves.add(bishopPoint);			
		}
		for(int x2=x,y2=y; x2>0 || y2<7; x2--,y2++) {
			if(x2==0 || y2==7)
				break;

			Point bishopPoint = new Point(x2-1, y2+1);
			bishopMoves.add(bishopPoint);		
		}
		
		return bishopMoves;		
	}	
}