package model;

import java.awt.Point;
import java.util.ArrayList;

public class King extends Piece {
	
	public King(String name, String piecePath) {
		super(name, piecePath);
	}
	public ArrayList<Point> getMoves(int x,int y){
		ArrayList<Point> KingMoves = new ArrayList<>();
		
		if (x==0){
			//pra baixo
			Point KingPoint = new Point(x+1, y);
			KingMoves.add(KingPoint);
		}
		if (x==0 && y!=7){
			//pra diagonal direita baixo
			Point KingPoint1 = new Point(x+1, y+1);
			KingMoves.add(KingPoint1);
			
			//pra direita
			Point KingPoint2 = new Point(x, y+1);
			KingMoves.add(KingPoint2);
			
		}
		if (x==0 && y!=0){
			//pra diagonal esquerda baixo
			Point KingPoint3 = new Point(x+1, y-1);
			KingMoves.add(KingPoint3);
			
			//pra esquerda
			Point KingPoint4 = new Point(x, y-1);
			KingMoves.add(KingPoint4);
		}
		if (x==7){
			//pra cima
			Point KingPoint5 = new Point(x-1, y);
			KingMoves.add(KingPoint5);
		}
		if (x==7 && y!=0){
			//diagonal esquerda cima
			Point KingPoint6 = new Point(x-1, y-1);
			KingMoves.add(KingPoint6);
			
			//pra esquerda
			Point KingPoint7 = new Point(x, y-1);
			KingMoves.add(KingPoint7);
		
		}
		if (x==7 && y!=7){
			//diagonal direita cima 
			Point KingPoint8 = new Point(x-1, y+1);
			KingMoves.add(KingPoint8);
			
			//pra direita
			Point KingPoint9 = new Point(x, y+1);
			KingMoves.add(KingPoint9);
		}
		
		if (x!=0 && x!=7){
			//pra cima
			Point KingPoint10 = new Point(x-1, y);
			KingMoves.add(KingPoint10);
			
			//pra baixo
			Point KingPoint11 = new Point(x+1, y);
			KingMoves.add(KingPoint11);
			
		}
		if (x!=0 && x!=7 && y!=0){
			//pra esquerda
			Point KingPoint12 = new Point(x, y-1);
			KingMoves.add(KingPoint12);
			
			//diagonal esquerda cima
			Point KingPoint13 = new Point(x-1, y-1);
			KingMoves.add(KingPoint13);
			
			//diagonal esquerda baixo
			Point KingPoint14 = new Point(x+1, y-1);
			KingMoves.add(KingPoint14);
		}
		if (x!=0 && x!=7 && y!=7){
			//pra direita
			Point KingPoint15 = new Point(x, y+1);
			KingMoves.add(KingPoint15);
			
			//diagonal direita cima
			Point KingPoint16 = new Point(x-1, y+1);
			KingMoves.add(KingPoint16);
			
			//diagonal direita baixo
			Point KingPoint17 = new Point(x+1, y+1);
			KingMoves.add(KingPoint17);
		
		}
		return KingMoves;		
	}
}