package model;

import java.awt.Point;
import java.util.ArrayList;

public class Knight extends Piece {

	public Knight(String name, String piecePath) {
		super(name, piecePath);
	}
	public ArrayList<Point> getMoves(int x,int y){
		ArrayList<Point> KnightMoves = new ArrayList<>();
		
		//1
		if(x-1<=7 && x-1 >=0 && y-2<=7 && y-2 >=0 ){
			Point KingPoint = new Point(x-1, y-2);
			KnightMoves.add(KingPoint);
		}
		//2
		if(x-1<=7 && x-1 >=0 && y+2<=7 && y+2 >=0 ){
			Point KingPoint2 = new Point(x-1, y+2);
			KnightMoves.add(KingPoint2);
		}
		//3
		if(x-2<=7 && x-2 >=0 && y-1<=7 && y-1 >=0 ){
			Point KingPoint3 = new Point(x-2, y-1);
			KnightMoves.add(KingPoint3);
		}
		//4
		if(x-2<=7 && x-2 >=0 && y+1<=7 && y+1 >=0 ){
			Point KingPoint4 = new Point(x-2, y+1);
			KnightMoves.add(KingPoint4);
		}
		//5
		if(x+2<=7 && x+2 >=0 && y+1<=7 && y+1 >=0 ){
			Point KingPoint5 = new Point(x+2, y+1);
			KnightMoves.add(KingPoint5);
		}
		//6
		if(x+2<=7 && x+2 >=0 && y-1<=7 && y-1 >=0 ){
			Point KingPoint6 = new Point(x+2, y-1);
			KnightMoves.add(KingPoint6);
		}
		//7
		if(x+1<=7 && x+1 >=0 && y-2<=7 && y-2 >=0 ){
			Point KingPoint7 = new Point(x+1, y-2);
			KnightMoves.add(KingPoint7);
		}
		//8
		if(x+1<=7 && x+1 >=0 && y+2<=7 && y+2 >=0 ){
			Point KingPoint8 = new Point(x+1, y+2);
			KnightMoves.add(KingPoint8);
		}
		System.out.println (x);
		System.out.println (y);
	
		return KnightMoves;		
	}		
}
