package model;

import java.awt.Point;
import java.util.ArrayList;

public class Pawn extends Piece {

	public Pawn(String name, String piecePath) {
		super(name, piecePath);
	}	
	public ArrayList<Point> getMovesW(int x,int y){
		ArrayList<Point> PawnWMoves = new ArrayList<>();
	if (x!=0){	
		if (x==6){
			Point PawnPoint = new Point(x-1, y);
			PawnWMoves.add(PawnPoint);
			
			Point PawnPoint2 = new Point(x-2, y);
			PawnWMoves.add(PawnPoint2);
		}
		if (x!=6 && x!=0){
			Point PawnPoint = new Point(x-1, y);
			PawnWMoves.add(PawnPoint);
		}
		if (y!=0){
			Point PawnPoint = new Point(x-1, y-1);
			PawnWMoves.add(PawnPoint);
		}
		if (y!=7){
			Point PawnPoint = new Point(x-1, y+1);
			PawnWMoves.add(PawnPoint);
		}
	}
		return PawnWMoves;
	}
	public ArrayList<Point> getMovesB(int x,int y){
		ArrayList<Point> PawnBMoves = new ArrayList<>();
		if (x!=7){	
			if (x==1){
				Point PawnPoint = new Point(x+1, y);
				PawnBMoves.add(PawnPoint);
			
				Point PawnPoint2 = new Point(x+2, y);
				PawnBMoves.add(PawnPoint2);
			}
			if (x!=1 && x!=7){
				Point PawnPoint = new Point(x+1, y);
				PawnBMoves.add(PawnPoint);
			}
			if (y!=0){
				Point PawnPoint = new Point(x+1, y-1);
				PawnBMoves.add(PawnPoint);
			}
			if (y!=7){
				Point PawnPoint = new Point(x+1, y+1);
				PawnBMoves.add(PawnPoint);
			}
		}
		return PawnBMoves;
	}

}
