package model;

import java.util.ArrayList;
import java.awt.Point;

import javax.swing.text.Position;

public abstract class Piece {
	private String pieceName;
	private String piecePath;
	private String pieceColor;
	private ArrayList<Position> moves;
	
	 public Piece(String name,String piecePath) {
		 super();
		 this.pieceName = name;
		 this.piecePath = piecePath;
	 }

	public String getPieceName() {
		return pieceName;
	}

	public void setPieceName(String pieceName) {
		this.pieceName = pieceName;
	}
	public String getPieceColor() {
		return pieceColor;
	}

	public void setPieceColor(String pieceColor) {
		this.pieceColor = pieceColor;
	}
	public String getPiecePath() {
		return piecePath;
	}

	public ArrayList<Position> getMoves() {
		return moves;
	}

	public void setMoves(ArrayList<Position> moves) {
		this.moves = moves;
	}

	public void setPiecePath(String piecePath) {
		this.piecePath = piecePath;
	}
	
	public ArrayList<Point> getMoves(int x, int y) {
		return null;
	}
	public ArrayList<Point> getMovesW(int x, int y) {
		return null;
	}
	public ArrayList<Point> getMovesB(int x, int y) {
		return null;
	}
}