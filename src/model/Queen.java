package model;

import java.awt.Point;
import java.util.ArrayList;

public class Queen extends Piece {

	public Queen(String name, String piecePath) {
		super(name, piecePath);
	}
	public ArrayList<Point> getMoves(int x,int y){
		ArrayList<Point> QueenMoves = new ArrayList<>();
		if (x==0 && y==0){
				for (int i=1; i<=7; i++){
						Point QueenPoint = new Point(i, y);
						QueenMoves.add(QueenPoint);
				}
				for (int j=1; j<=7; j++){
					Point QueenPoint2 = new Point(x, j);
					QueenMoves.add(QueenPoint2);
				}
		}
		if (x==0 && y==7){
				for (int i=1; i<=7; i++){
						Point QueenPoint3 = new Point(i, y);
						QueenMoves.add(QueenPoint3);
				}
				for (int j=7; j>=0; j--){
					Point QueenPoint4 = new Point(x, j);
					QueenMoves.add(QueenPoint4);
				}
		}
		if (x==7 && y==0){
			for (int i=6; i>=0; i--){
					Point QueenPoint5 = new Point(i, y);
					QueenMoves.add(QueenPoint5);
			}
			for (int j=0; j<=7; j++){
				Point QueenPoint6 = new Point(x, j);
				QueenMoves.add(QueenPoint6);
			}
		}
		if (x==7 && y==7){
			for (int i=6; i>=0; i--){
					Point QueenPoint7 = new Point(i, y);
					QueenMoves.add(QueenPoint7);
			}
			for (int j=7; j>=0; j--){
				Point QueenPoint8 = new Point(x, j);
				QueenMoves.add(QueenPoint8);
			}
		}
		if (x!=0 && x!=7 && y==0){
			for (int j=0; j<=7; j++){
				Point QueenPoint9 = new Point(x, j);
				QueenMoves.add(QueenPoint9);
			}
			for (int i=x; i>=0; i--){
				Point QueenPoint10 = new Point(i, y);
				QueenMoves.add(QueenPoint10);
			}
			for (int z=7; z>x; z--){
				Point QueenPoint11 = new Point(z, y);
				QueenMoves.add(QueenPoint11);
			}
		}
		if (x!=0 && x!=7 && y==7){
			for (int j=7; j>=0; j--){
				Point QueenPoint12 = new Point(x, j);
				QueenMoves.add(QueenPoint12);
			}
			for (int i=x; i>=0; i--){
				Point QueenPoint13 = new Point(i, y);
				QueenMoves.add(QueenPoint13);
			}
			for (int z=7; z>x; z--){
				Point QueenPoint14 = new Point(z, y);
				QueenMoves.add(QueenPoint14);
			}
		}
		if (x==0 && y!=7 && y!=0){
			for (int j=0; j<=7; j++){
				Point QueenPoint15 = new Point(j, y);
				QueenMoves.add(QueenPoint15);
			}
			for (int i=y; i>=0; i--){
				Point QueenPoint16 = new Point(x, i);
				QueenMoves.add(QueenPoint16);
			}
			for (int z=y; z<=7; z++){
				Point QueenPoint17 = new Point(x, z);
				QueenMoves.add(QueenPoint17);
			}
		}
		if (x==7 && y!=7 && y!=0){
			for (int j=7; j>=0; j--){
				Point QueenPoint18 = new Point(j, y);
				QueenMoves.add(QueenPoint18);
			}
			for (int i=y; i>=0; i--){
				Point QueenPoint19 = new Point(x, i);
				QueenMoves.add(QueenPoint19);
			}
			for (int z=y; z<=7; z++){
				Point QueenPoint20 = new Point(x, z);
				QueenMoves.add(QueenPoint20);
			}
		}
		if (x!=0 && x!=7 && y!=0 && y!=7) {
			for (int i=y; i>=0; i--){
				Point QueenPoint21 = new Point(x, i);
				QueenMoves.add(QueenPoint21);
			}
			for (int z=y; z<=7; z++){
				Point QueenPoint22 = new Point(x, z);
				QueenMoves.add(QueenPoint22);
			}
			for (int i=x; i>=0; i--){
				Point QueenPoint23 = new Point(i, y);
				QueenMoves.add(QueenPoint23);
			}
			for (int z=x; z<=7; z++){
				Point QueenPoint24 = new Point(z, y);
				QueenMoves.add(QueenPoint24);
			}
		}
		for(int x2=x,y2=y; x2<7 || y2<7 ; x2++,y2++) {
			if(x2==7 || y2==7)
				break;		
						
			Point QueenPoint = new Point(x2+1, y2+1);
			QueenMoves.add(QueenPoint);			
		}

		for(int x2=x,y2=y; x2<7 || y2>0; x2++,y2--) {
			if(x2==7 || y2==0)
				break;
			
			Point QueenPoint = new Point(x2+1, y2-1);
			QueenMoves.add(QueenPoint);			
		}

		for(int x2=x,y2=y; x2>0 || y2<7; x2--,y2++) {
			if(x2==0 || y2==7)
				break;

			Point QueenPoint = new Point(x2-1, y2+1);
			QueenMoves.add(QueenPoint);		
		}
		
		for(int x2 = x, y2 = y; x2>0 || y2>0; x2--, y2--) {
			if(x==0 || y==0)
				break;

			Point QueenPoint = new Point(x2-1, y2-1);
			QueenMoves.add(QueenPoint);			
		}
	
		return QueenMoves;		
	}
}