package model;

import java.awt.Point;
import java.util.ArrayList;

public class Rook extends Piece {

	public Rook(String name, String piecePath) {
		super(name, piecePath);
	}
	
	public ArrayList<Point> getMoves(int x,int y){
		ArrayList<Point> RookMoves = new ArrayList<>();
		
		if (x==0 && y==0){
				for (int i=1; i<=7; i++){
						Point RookPoint = new Point(i, y);
						RookMoves.add(RookPoint);
				}
				for (int j=1; j<=7; j++){
					Point RookPoint2 = new Point(x, j);
					RookMoves.add(RookPoint2);
				}
		}
		if (x==0 && y==7){
				for (int i=1; i<=7; i++){
						Point RookPoint3 = new Point(i, y);
						RookMoves.add(RookPoint3);
				}
				for (int j=7; j>=0; j--){
					Point RookPoint4 = new Point(x, j);
					RookMoves.add(RookPoint4);
				}
		}
		if (x==7 && y==0){
			for (int i=6; i>=0; i--){
					Point RookPoint5 = new Point(i, y);
					RookMoves.add(RookPoint5);
			}
			for (int j=0; j<=7; j++){
				Point RookPoint6 = new Point(x, j);
				RookMoves.add(RookPoint6);
			}
		}
		if (x==7 && y==7){
			for (int i=6; i>=0; i--){
					Point RookPoint7 = new Point(i, y);
					RookMoves.add(RookPoint7);
			}
			for (int j=7; j>=0; j--){
				Point RookPoint8 = new Point(x, j);
				RookMoves.add(RookPoint8);
			}
		}
		if (x!=0 && x!=7 && y==0){
			for (int j=0; j<=7; j++){
				Point RookPoint9 = new Point(x, j);
				RookMoves.add(RookPoint9);
			}
			for (int i=x; i>=0; i--){
				Point RookPoint10 = new Point(i, y);
				RookMoves.add(RookPoint10);
			}
			for (int z=7; z>x; z--){
				Point RookPoint11 = new Point(z, y);
				RookMoves.add(RookPoint11);
			}
		}
		if (x!=0 && x!=7 && y==7){
			for (int j=7; j>=0; j--){
				Point RookPoint12 = new Point(x, j);
				RookMoves.add(RookPoint12);
			}
			for (int i=x; i>=0; i--){
				Point RookPoint13 = new Point(i, y);
				RookMoves.add(RookPoint13);
			}
			for (int z=7; z>x; z--){
				Point RookPoint14 = new Point(z, y);
				RookMoves.add(RookPoint14);
			}
		}
		if (x==0 && y!=7 && y!=0){
			for (int j=0; j<=7; j++){
				Point RookPoint15 = new Point(j, y);
				RookMoves.add(RookPoint15);
			}
			for (int i=y; i>=0; i--){
				Point RookPoint16 = new Point(x, i);
				RookMoves.add(RookPoint16);
			}
			for (int z=y; z<=7; z++){
				Point RookPoint17 = new Point(x, z);
				RookMoves.add(RookPoint17);
			}
		}
		if (x==7 && y!=7 && y!=0){
			for (int j=7; j>=0; j--){
				Point RookPoint18 = new Point(j, y);
				RookMoves.add(RookPoint18);
			}
			for (int i=y; i>=0; i--){
				Point RookPoint19 = new Point(x, i);
				RookMoves.add(RookPoint19);
			}
			for (int z=y; z<=7; z++){
				Point RookPoint20 = new Point(x, z);
				RookMoves.add(RookPoint20);
			}
		}
		if (x!=0 && x!=7 && y!=0 && y!=7) {
			for (int i=y; i>=0; i--){
				Point RookPoint21 = new Point(x, i);
				RookMoves.add(RookPoint21);
			}
			for (int z=y; z<=7; z++){
				Point RookPoint22 = new Point(x, z);
				RookMoves.add(RookPoint22);
			}
			for (int i=x; i>=0; i--){
				Point RookPoint23 = new Point(i, y);
				RookMoves.add(RookPoint23);
			}
			for (int z=x; z<=7; z++){
				Point RookPoint24 = new Point(z, y);
				RookMoves.add(RookPoint24);
			}
		}
	
		return RookMoves;		
	}

}