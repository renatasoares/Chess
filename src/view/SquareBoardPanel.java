package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import model.*;

import javax.swing.JPanel;

import control.SquareControl;

public class SquareBoardPanel extends JPanel {

	private static final long serialVersionUID = 7332850110063699836L;

	private SquareControl squareControl;
	private ArrayList<SquarePanel> squarePanelList;

	public SquareBoardPanel() {
		setLayout(new GridBagLayout());
		this.squarePanelList = new ArrayList<SquarePanel>();

		initializeSquareControl();
		initializeGrid();
		initializePiecesInChess();
	}

	private void initializeSquareControl() {
		Color colorOne = Color.WHITE;
		Color colorTwo = Color.GRAY;
		Color colorHover = Color.BLUE;
		Color colorSelected = Color.GREEN;
		Color colorMoves = Color.GREEN;

		this.squareControl = new SquareControl(colorOne, colorTwo, colorHover,
				colorSelected, colorMoves);
	}

	private void initializeGrid() {
		GridBagConstraints gridBag = new GridBagConstraints();

		Square square;
		for (int i = 0; i < this.squareControl.getSquareList().size(); i++) {
			square = this.squareControl.getSquareList().get(i);
			gridBag.gridx = square.getPosition().y;
			gridBag.gridy = square.getPosition().x;

			SquarePanel squarePanel = new SquarePanel(square);

			add(squarePanel, gridBag);
			this.squarePanelList.add(squarePanel);
		}

	}

	private void initializePiecesInChess() {
		Piece brownPawn = new Pawn("PawnB","icon/Brown P_48x48.png");
		brownPawn.setPieceColor("BROWN");
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(1, i).setPiece(brownPawn);
		}

		Piece brownRook = new Rook("RookB","icon/Brown R_48x48.png");
		brownRook.setPieceColor("BROWN");
		this.squareControl.getSquare(0, 0).setPiece(brownRook);
		this.squareControl.getSquare(0, 7).setPiece(brownRook);

		Piece brownKnight = new Knight("KnightB","icon/Brown N_48x48.png"); 
		brownKnight.setPieceColor("BROWN");
		this.squareControl.getSquare(0, 1).setPiece(brownKnight);
		this.squareControl.getSquare(0, 6).setPiece(brownKnight);

		Piece brownBishop = new Bishop("BishopB","icon/Brown B_48x48.png");
		brownBishop.setPieceColor("BROWN");
		this.squareControl.getSquare(0, 2).setPiece(brownBishop);
		this.squareControl.getSquare(0, 5).setPiece(brownBishop);

		Piece brownQueen = new Queen("QueenB","icon/Brown Q_48x48.png"); 
		brownQueen.setPieceColor("BROWN");
		this.squareControl.getSquare(0, 4).setPiece(brownQueen);

		Piece brownKing = new King("KingB","icon/Brown K_48x48.png");
		brownKing.setPieceColor("BROWN");
		this.squareControl.getSquare(0, 3).setPiece(brownKing);
		
		Piece whitePawn = new Pawn("PawnW","icon/White P_48x48.png");
		whitePawn.setPieceColor ("WHITE");
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(6, i).setPiece(whitePawn);
		}
		
		Piece whiteRook = new Rook("RookW","icon/White R_48x48.png");
		whiteRook.setPieceColor ("WHITE");
		this.squareControl.getSquare(7, 0).setPiece(whiteRook);
		this.squareControl.getSquare(7, 7).setPiece(whiteRook);

		Piece whiteKnight = new Knight("KnightW","icon/White N_48x48.png");
		whiteKnight.setPieceColor ("WHITE");
		this.squareControl.getSquare(7, 1).setPiece(whiteKnight);
		this.squareControl.getSquare(7, 6).setPiece(whiteKnight);

		Piece whiteBishop = new Bishop("BishopW","icon/White B_48x48.png");
		whiteBishop.setPieceColor ("WHITE");
		this.squareControl.getSquare(7, 2).setPiece(whiteBishop);
		this.squareControl.getSquare(7, 5).setPiece(whiteBishop);

		Piece whiteQueen = new Queen("QueenW","icon/White Q_48x48.png");
		whiteQueen.setPieceColor ("WHITE");
		this.squareControl.getSquare(7, 4).setPiece(whiteQueen);

		Piece whiteKing = new King("KingW","icon/White K_48x48.png");
		whiteKing.setPieceColor ("WHITE");
		this.squareControl.getSquare(7, 3).setPiece(whiteKing);
	}
}
